package C2_UD20_T20_2.C2_UD20_T20_2;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JButton;

public class mainApp extends JFrame {

	private JPanel contentPane;

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					
					mainApp frame = new mainApp();
					frame.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
				
			}
			
		});
		
	}

	public mainApp() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 430, 180);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel mainLabel = new JLabel("Has pulsado:");
		sl_contentPane.putConstraint(SpringLayout.NORTH, mainLabel, 14, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, mainLabel, 70, SpringLayout.WEST, contentPane);
		contentPane.add(mainLabel);
		
		final JLabel pressedButtonLabel = new JLabel("");
		sl_contentPane.putConstraint(SpringLayout.NORTH, pressedButtonLabel, 0, SpringLayout.NORTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, pressedButtonLabel, 6, SpringLayout.EAST, mainLabel);
		contentPane.add(pressedButtonLabel);
		
		JButton firstButton = new JButton("Boton 1");
		sl_contentPane.putConstraint(SpringLayout.NORTH, firstButton, -4, SpringLayout.NORTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, firstButton, 6, SpringLayout.EAST, pressedButtonLabel);
		contentPane.add(firstButton);
		
		JButton secondButton = new JButton("Boton 2");
		sl_contentPane.putConstraint(SpringLayout.NORTH, secondButton, -4, SpringLayout.NORTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, secondButton, 6, SpringLayout.EAST, firstButton);
		contentPane.add(secondButton);
		
		firstButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				pressedButtonLabel.setText("Boton 1");
				
			}
			
		});
		
		secondButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				pressedButtonLabel.setText("Boton 2");
				
			}
			
		});
		
	}
	
}
